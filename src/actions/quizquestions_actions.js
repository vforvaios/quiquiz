import {
  GET_QUIZ_QUESTIONS,
  NEXT_QUESTION,
  CALC_SCORE,
  RESET_EVERYTHING,
  apiUrl,
  amountOfQuestions,
  questions_type } from '../config'

import axios from 'axios'
import { randomizeArray } from '../utils'
import { handleLoading } from './loading_actions'

export const resetSSQ = () => {
  return {
    type: RESET_EVERYTHING,
    score: 0,
    selectedquestion: 1
  }
}

export const fetchQuestionsDataSuccessful = (data) => {
  return {
    type: GET_QUIZ_QUESTIONS,
    data
  }
}

export const fetchQuestionsData = (category_id) => {
  return dispatch => {

    dispatch(handleLoading(true))

    axios.get(`${apiUrl}?amount=${amountOfQuestions}&category=${category_id}&type=${questions_type}`,
      {
        headers: {
          "Content-Type" : "application/json; charset=UTF-8"
        }
      })
      .then( ( response ) => {
        // RANDOMIZE ANSWERS AND ADDING THEM TO THEIR OWN PROPERTY BEFORE RETURN RESULTS
        let results = response.data.results.map(result => {
          let tempanswers = []
          result.incorrect_answers.map(inc_ans => tempanswers.push(inc_ans))
          tempanswers.push(result.correct_answer)
          tempanswers = randomizeArray(tempanswers)

          return {...result, allanswers: tempanswers }
        })

        dispatch(fetchQuestionsDataSuccessful(results))
      } )
      .then( () => {
        dispatch(handleLoading(false))
      } )
      .catch( ( error ) => {
        console.log(error)
        dispatch(handleLoading(false))
      } )
  }
}

export const nextQuestion = () => {
  return {
    type: NEXT_QUESTION,
    payload: 1
  }
}

export const calcScore = (score) => {
  return {
    type: CALC_SCORE,
    score
  }
}