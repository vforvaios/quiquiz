import {
  LOADING
} from '../config'

export const handleLoading = (loading) => {
  return {
    type: LOADING,
    loading
  }
}