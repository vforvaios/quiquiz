import axios from 'axios'
import { handleLoading } from './loading_actions'
import {
  apiCategoriesUrl,
  GET_QUIZ_CATEGORIES
} from '../config'

export const getQuizCategoriesSuccessful = (categories) => {
  return {
    type: GET_QUIZ_CATEGORIES,
    categories
  }
}

export const getQuizCategories = () => {
  return dispatch => {
    dispatch(handleLoading(true))

    axios.get(apiCategoriesUrl)
    .then( response => {
      dispatch(getQuizCategoriesSuccessful(response.data.trivia_categories))
    } )
    .then( () => {
      dispatch(handleLoading(false))
    } )
    .catch( error => {
      console.log(error)
      dispatch(handleLoading(false))
    } )
  }
}