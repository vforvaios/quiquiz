import React from 'react';

// packages
import { Provider } from "react-redux";
import store from "./store/store";
import { BrowserRouter as Router, Route } from 'react-router-dom';

// components
import WelcomeScreen from './components/WelcomeScreen';
import QuizCategories from './components/QuizCategories';
import Questions from './components/Questions';
import Loader from './components/Loader';
import './main.scss'

const App = () => {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <div>
            <Loader />
            <Route exact path="/" component={WelcomeScreen}></Route>
            <Route path="/categories" component={QuizCategories}></Route>
            <Route exact path="/questions/:id" component={Questions}></Route>
          </div>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
