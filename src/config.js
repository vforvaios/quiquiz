//constants
const GET_QUIZ_CATEGORIES = 'GET_QUIZ_CATEGORIES'
const GET_QUIZ_QUESTIONS = 'GET_QUIZ_QUESTIONS'
const NEXT_QUESTION = 'NEXT_QUESTION'
const CALC_SCORE = 'CALC_SCORE'
const LOADING = 'LOADING'
const RESET_EVERYTHING = 'RESET_EVERYTHING'

const apiUrl = "https://opentdb.com/api.php"
const apiCategoriesUrl = "https://opentdb.com/api_category.php"
const amountOfQuestions = 10
const questions_type = 'multiple'

const scoreDifficulty = {
  easy: 10,
  medium: 20,
  hard: 30
}

export {
  GET_QUIZ_CATEGORIES,
  GET_QUIZ_QUESTIONS,
  NEXT_QUESTION,
  CALC_SCORE,
  LOADING,
  RESET_EVERYTHING,
  apiUrl,
  apiCategoriesUrl,
  amountOfQuestions,
  questions_type,
  scoreDifficulty
}