import { GET_QUIZ_CATEGORIES } from '../config'

const initialState = {
  trivia_categories: []
}

const quizcategories_reducer = (state = initialState, action) => {
  let newState

  switch (action.type) {
    case GET_QUIZ_CATEGORIES:
      newState = {...state, trivia_categories: [...action.categories]}
      return newState

    default:
      return state
  }
}

export default quizcategories_reducer

