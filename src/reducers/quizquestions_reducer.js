import {
  GET_QUIZ_QUESTIONS,
  NEXT_QUESTION,
  CALC_SCORE,
  RESET_EVERYTHING } from '../config'

const initialState = {
  questions: [],
  selectedquestion: 1,
  score: 0
}

const quizquestions_reducer = (state = initialState, action) => {
  let newState

  switch(action.type) {
    case GET_QUIZ_QUESTIONS:
      newState = {...state, questions: [...action.data]}
      return newState

    case NEXT_QUESTION:
      newState = {...state, selectedquestion: state.selectedquestion + action.payload}
      return newState

    case CALC_SCORE:
      newState = {...state, score: state.score + action.score}
      return newState

    case RESET_EVERYTHING:
      newState = {...state, score: action.score, selectedquestion: action.selectedquestion}
      return newState

    default:
      return state
  }
}

export default quizquestions_reducer