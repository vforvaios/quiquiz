import {
  LOADING
} from '../config'

const initialState = {
  isLoading: false
}

const loader_reducer = (state = initialState, action) => {
  let newState

  switch (action.type) {
    case LOADING:
      newState = {...state, isLoading: action.loading}
      return newState

    default:
      return state
  }
}

export default loader_reducer