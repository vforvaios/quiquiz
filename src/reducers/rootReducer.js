import { combineReducers } from 'redux'
import quizcategories_reducer from './quizcategories_reducer'
import quizquestions_reducer from './quizquestions_reducer'
import loader_reducer from './loader_reducer'

const rootReducer = combineReducers({
    quizcategories_reducer,
    quizquestions_reducer,
    loader_reducer
  })

export default rootReducer