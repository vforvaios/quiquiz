import React from 'react';

import { NavLink } from 'react-router-dom';
import '../scss/welcome.scss'

const WelcomeScreen = () => {
  return (
    <React.Fragment>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 text-center p-3">
            <h1>Welcome Screen</h1>
          </div>
        </div>
      </div>
      <NavLink className="mainplaybutton" to="/categories">
        PLAY NOW!
      </NavLink>
 
    </React.Fragment>
  );
};

export default WelcomeScreen;
