import React, { Component } from 'react';
import { connect } from 'react-redux'
import { calcScore } from '../actions/quizquestions_actions'
import { scoreDifficulty } from '../config'
import '../scss/answers.scss'

class Question extends Component {
    
  constructor(props) {
    super(props);

    this.state = {
      disableallanswers: false
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.disableallanswers) {
      this.setState({ disableallanswers: false }, () => {
        this.props.enableButtonNext()
      })
    }
  }

  handleCheckAnswer = (e) => {
    const { correct_answer, difficulty } = this.props.question
    let scoreToAdd = 0
    
    if (e.currentTarget.textContent === correct_answer) {
      scoreToAdd = scoreDifficulty[difficulty]
      this.props.calculateScore(scoreToAdd)
    } else {
      alert('WROOOONG !')
    }

    this.handleDisableAnswers()
  }

  handleDisableAnswers = () => {
    this.setState({ disableallanswers: true }, () => {
      this.props.enableButtonNext()
    })
  }

  render() {
    const correct_answer = this.props.question.correct_answer
    const { disableallanswers } = this.state
    const { question: q } = this.props
    const disabledClass = disableallanswers ? 'disabledanswer': ''

    let actual_question = q.question.replace(/&#039;/g, '')
                                    .replace(/&quot;/g, '')
                                    .replace(/&Uuml;/g, 'ü')
                                    .replace(/&amp;/g, '&')
    let classtoshow = ''

    return (
      <div className="question-wrapper">
        <h5 className="question mb-4">
          {actual_question}
        </h5>

        <div className="answers-wrapper mb-4">
          {
            q.allanswers.map( (answer, index) => {
              classtoshow = (answer === correct_answer && disableallanswers) ? 'correct-answer' : ''
              return <div 
                        key={index}
                        className={`answer mb-2 ${disabledClass} ${classtoshow}`}
                        onClick={this.handleCheckAnswer}>
                          {
                            answer.replace(/&#039;/g, '')
                                  .replace(/&quot;/g, '')
                                  .replace(/&Uuml;/g, 'ü')
                                  .replace(/&amp;/g, '&')
                          }
                      </div>
            })
          }
        </div>
      </div>    
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    calculateScore: score => { dispatch(calcScore(score)) }
  }
}

export default connect(null, mapDispatchToProps)(Question)
