import React from 'react';
import { NavLink } from 'react-router-dom';

import '../scss/category.scss'

const Category = ({ category }) => {
  return (
    <div className="category col-lg-3 col-md-6 mb-3 text-center">
      <NavLink className="navlinks" to={`/questions/${category.id}`}>
        <span>{category.name}</span>
      </NavLink>
    </div>
  );
};

export default Category;
