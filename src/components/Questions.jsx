import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom';

import '../scss/score.scss'
import Question from './Question'
import { fetchQuestionsData, nextQuestion } from '../actions/quizquestions_actions'
import { amountOfQuestions } from '../config'

class Questions extends Component {

  constructor(props) {
    super(props)

    this.state = {
      showScore: false,
      enableButton: false
    }
  }

  showScore = () => {
    this.setState({ showScore: true })
  }

  componentDidMount() {
    const category_id = this.props.match.params.id
    this.props.fetchQuestions(category_id)
  }

  componentDidUpdate(prevProps) {
    
    if(prevProps.selectedquestion !== this.props.selectedquestion) {
      this.handleNextVisibility()
    }
  }

  handleNextVisibility = () => {
    this.setState({ enableButton: !this.state.enableButton })
  }

  render() {
    const { showScore, enableButton } = this.state
    const { questions, selectedquestion } = this.props
    const lastElement = (
      selectedquestion === amountOfQuestions
        ? <button className={`next-question ${enableButton}`} onClick={this.showScore}>SHOW ME MY SCORE!</button>
        : <button className={`next-question ${enableButton}`} onClick={this.props.goToNextQuestion}>Next question</button>
    )
    
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <h1 className="text-center m-3">Questions screen</h1>
          </div>
        </div>

        <div className="row">
          <div className="col-10 offset-1">
            {
              questions.filter((question, index) => index+1 === selectedquestion).map((q, i) => {
                return (
                  <Question enableButtonNext={this.handleNextVisibility} question={q} key={i} />
                )
              })
            }

          </div>
        </div>

        <div className="row">
          <div className="col-10 offset-1">
            {lastElement}
          </div>
        </div>

        <div className={`row showScore ${showScore}`}>
          <div className="col-12">
            <div className="final-score">
              <NavLink className="button play-again text-center mb-3" to="/categories">PLAY AGAIN?</NavLink>
              <div>{this.props.score}</div>
            </div>
          </div>
        </div>
      </div>
        
    )
  }
}

const mapStateToProps = state => {
  return {
    questions: state.quizquestions_reducer.questions,
    selectedquestion: state.quizquestions_reducer.selectedquestion,
    score: state.quizquestions_reducer.score
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchQuestions: id => { dispatch(fetchQuestionsData(id)) },
    goToNextQuestion: () => { dispatch(nextQuestion()) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Questions)
