import React, { Component } from 'react';
import { connect } from 'react-redux'

import Category from './Category'

import { getQuizCategories } from '../actions/quizcategories_actions'
import { resetSSQ } from '../actions/quizquestions_actions'

class QuizCategories extends Component {

  componentDidMount() {
    this.props.fetchCategories()
    this.props.resetEverything()
  }

  render() {
    const {trivia_categories} = this.props

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <h1 className="text-center m-3">QuizCategories screen</h1>
          </div>
        </div>

        <div className="row justify-content-between align-items-center">
          {
            trivia_categories.map( category => {
              return <Category key={category.id} category={category} />
            } )
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    trivia_categories: state.quizcategories_reducer.trivia_categories
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchCategories: () => { dispatch(getQuizCategories()) },
    resetEverything: () => { dispatch(resetSSQ()) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuizCategories)
