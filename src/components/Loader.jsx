import React, { Component } from 'react'
import { connect } from 'react-redux'
import "../scss/loader.scss";

class Loader extends Component {
  render() {
    let loaderComponent;

    if (this.props.isLoading) {
      loaderComponent = (
        <div className="loading-wrapper">
          <div className="loading-icon">
            <div className="actual-loading-icon"></div>
          </div>
        </div>
      )
    } else {
      loaderComponent = ''
    }

    return loaderComponent

  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.loader_reducer.isLoading
  }
}

export default connect(mapStateToProps)(Loader)