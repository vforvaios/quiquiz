//THE STORE HOLDS ALL THE STATE WE NEED!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// createStore is the function for creating Redux Store.
import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';

//createStore function takes a rootReducer as its first parameter
//reducers produce the state (change it e.g.)
//A reducer is just a Javascript function.
//A reducer takes two parameters: the current state and an action (more about actions soon).
import rootReducer from "../reducers/rootReducer";
const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;